﻿using System;

namespace assigning_variables
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine ("==================================");
            Console.WriteLine ("===============Hello==============");
            Console.WriteLine ("==================================");
            Console.WriteLine();
            var num1 = 3;
            var num2 = 31;
            
            Console.WriteLine($"The value of num1 = {num1}");
            Console.WriteLine();
            
            Console.WriteLine($"num1 + num2 = {num1 + num2}");
            Console.WriteLine();
            
            var num3 = 6;
            
            Console.WriteLine($"The new value of num1 = {num1 = num1 + num2 + num3}");
            Console.WriteLine();

            var strvar = "I am thinking..."; // String, not integer or doubles (not a number.)

            Console.WriteLine($"{strvar * 3}"); // Trying to purposefully multiple string by 3 which is reserved for integers and numbers, since you cannot multiple the alphabet without representing them mathmatically
        }
    }
}
